<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0">
<title>Home | CLink</title>
<link rel="stylesheet" href="/assets/css/global/colors.css">
<link rel="stylesheet" href="/assets/css/global/fonts.css">
<link rel="stylesheet" href="/frameworks/vNav/style.css">
<link rel="stylesheet" href="/assets/css/blocks/home/layout.css">
<link rel="stylesheet" href="/assets/css/blocks/home/content.css">


<!--____________shared_base_styles global responsive____________-->
<link rel="stylesheet" media = "screen and (min-width:1000px)" href="/assets/css/blocks/home/large.css">
<link rel="stylesheet" media = "screen and (min-width:600px) and (max-width:999px)" href="/assets/css/blocks/home/medium.css">
<link rel="stylesheet" media = "screen and (max-width:599px)" href="/assets/css/blocks/home/small.css">
<!--_____________________________________-->

<link rel="icon" href="/assets/imgs/favicon.svg" type="image/x-icon">

<script src="/frameworks/vNav/navigator.js"> </script>
<script src="<?=$app->config->assetLinks->live["vUx"]?>" data-modules="formComponents"> </script>
<script data-src="/assets/js/pages/index/index.js"> </script>
