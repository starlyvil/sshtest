// JS codes for index here
(function(){
    centerBox();
    switchInput();
	swapTarget();
})()

function centerBox(){
    $$.sm(searchContent).center();
}
function switchInput(){
    var CustomFormComponentsObj = new FormComponents(); 
    var slideSwitch = CustomFormComponentsObj.slideSwitch();
	slideSwitch.config.className = "slideSwitch";
	slideSwitch.config.sizeAttribute = "data-dim";
	slideSwitch.config.labelAttribute = "data-labels"; // "Onlabel, OffLabel"
    slideSwitch.config.wrapperStyles = ["margin-top:8px; border:solid 1px #53565e; margin-right:10px", "", ""];
	slideSwitch.config.showLabel = true;
	slideSwitch.config.labelStyles = ["background-color:#215f97;padding-right:7px;line-height:28px; color:white;", "background-color:#215f97; padding-left:7px; line-height:28px; color:white"];
	slideSwitch.config.handleStyles = ["width:18px; height:18px;background-image: linear-gradient(to bottom, #d5cfcf 0%, white 100%);border: solid 0.1px white;", "background-image: linear-gradient(to bottom, #d5cfcf 0%, white 100%);border: solid 0.1px white;"];
	slideSwitch.config.slideDistance = ["2px", "CALC(100% - 21px)"];
	slideSwitch.autoBuild();
}
function swapTarget(){
	var target = searchContent.querySelector("p span");
	setState(target);
	targetObject.addEventListener("change", function (e){
		setState(target);
	})
}

function setState(target){
	if(targetObject.checked){//freelancer
		target.innerText = "freelancers..."
	}else{//cpompany
		target.innerText = "companies..."
	};
}